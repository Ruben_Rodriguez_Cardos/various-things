class Q {
	int n;
	boolean valueSet = false;

	synchronized int get() {
		if (!valueSet)
			try {
				wait();
			} catch (InterruptedException e) {
				System.out.println("InterruptedException capturada");
			}
		System.out.println("Obtengo: " + n);
		valueSet = false;
		notify();
		return n;
	}

	synchronized void put(int n) {
		if (valueSet)
			try {
				wait();
			} catch (InterruptedException e) {
				System.out.println("InterruptedException capturada");
			}
		this.n = n;
		valueSet = true;
		System.out.println("Pongo: " + n);
		notify();
	}
}

class Producer implements Runnable {
	Q q;

	Producer(Q q) {
		this.q = q;
		// new Thread(this, "Productor").start();
	}

	public void run() {
		int i = 0;
		/*while (true) {
			q.put(i++);
		}*/
		while (i<10) {
			q.put(i++);
		}
	}
}

class Consumer implements Runnable {
	Q q;

	Consumer(Q q) {
		this.q = q;
		// new Thread(this, "Consumidor").start();
	}

	public void run() {
		/*while (true) {
			q.get();
		}*/
		int i = 0;
		while (i<10) {
			q.get();
			i++;
		}
	}
}

class ExclusionMutua {
	public static void main(String args[]) {
		Q q = new Q();
		Producer e = new Producer(q);
		Consumer c = new Consumer(q);
		Thread t1 = new Thread(e);
		Thread t2 = new Thread(c);
		t1.start();
		t2.start();
		// System.out.println("Pulsa Control-C para parar.");
	}
}