import java.util.concurrent.Semaphore;

class Buffer{
	
	int n;
	int init;
	
	private Semaphore semaphore_empty;
	private Semaphore semaphore_full;
	private Semaphore mutex;
	
	public Buffer(int n){
		this.n=n;
		init=5;
		
		//Inicializacion Semaforos
		semaphore_empty= new Semaphore(0);
		semaphore_full = new Semaphore(0);
		mutex = new Semaphore(1);
	}
	
	synchronized int consultar() {
		return n;
	}
	
	synchronized int consumir() {
		n--;
		return n;
	}

	synchronized void producir() {
		n=init;
			
	}
	
	synchronized Semaphore getSempahoreEmpty(){
		return semaphore_empty;
	}
	
	synchronized Semaphore getSempahoreFull(){
		return semaphore_full;
	}
	
	synchronized Semaphore getMutex(){
		return mutex;
	}
}

class P implements Runnable{
	
	private Buffer b;

	
	public P(Buffer b){
		this.b=b;
	}
	
	public void run(){
		//si estoy despierto produzco
		Semaphore empty = b.getSempahoreEmpty();
		Semaphore full = b.getSempahoreFull();
		while(true){
			try {
				empty.acquire();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("");
			System.out.println("--------------");
			System.out.println("Produciendo...");
			b.producir();
			full.release();
			System.out.println("Producidos" + b.consultar()+" recursos...");
			System.out.println("--------------");
		}
		
		
		//despues de producir duermo
	}
	
}

class C implements Runnable{

	private Buffer b;

	public C(Buffer b){
		this.b=b;
	}

	public void run(){
		Semaphore empty = b.getSempahoreEmpty();
		Semaphore full = b.getSempahoreFull();
		Semaphore mutex = b.getMutex();
		while(true){
			try {
				mutex.acquire();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.out.println("");
			System.out.println("--------------");
			System.out.println("Consumiendo...");
			if(b.consultar()<=0){
				//Alerto al productor
				System.out.println("Buffer vacio, produciendo...");
				empty.release();
				try {
					full.acquire();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			//Consumo
			int aux=b.consumir();
			System.out.println("Recursos restantes en el buffer: "+aux);
			System.out.println("Recurso consumido...");
			System.out.println("--------------");
			mutex.release();
		}	
	}
}

class ProductorConsumidor {

	public static void main(String[] args) {
		Buffer b = new Buffer(5);
		C c=new C(b);
		P p = new P(b);
		Thread t1 = new Thread(c);
		Thread t2 = new Thread(p);
		t1.start();
		t2.start();

	}

}
