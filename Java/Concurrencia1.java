
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Concurrencia1  {
	
	public static void main(String[] args) {
		
		Random r = new Random(System.currentTimeMillis());
		for(int i =0;i<2;i++){
			try {
				Hilo h = new Hilo(i,r.nextInt(1000));//clase que se ejecuta en el hilo
				Thread t = new Thread(h);//Hilo(clase a ejecutar en el hilo)
				t.start();//Inicio la ejecucion
				t.join();//Fuerzo a que se espere a que termine un hilo
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		/*int m[] = new int[3];
		for(int i =0;i<m.length;i++){
			Hilo3 h = new Hilo3(i,m);//clase que se ejecuta en el hilo
			Thread t = new Thread(h);//Hilo(clase a ejecutar en el hilo)
			t.start();//Inicio la ejecucion
			//t.join();//Fuerzo a que se espere a que termine un hilo
		}
		for(int i =0;i<m.length;i++){
			System.out.print(m[i]+" ");
		}
		
		System.out.println("---EXECUTOR---");
		
		//Otra forma
		ExecutorService executor = Executors.newFixedThreadPool(3);
		for(int i =0;i<3;i++){
			try {
				executor.execute(new Hilo2(i));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		executor.shutdown();
		while (!executor.isTerminated()) {
			System.out.println("En ejecucion..."); 
		}*/
		System.out.println("Acabado");
	}
}

class Hilo3 implements Runnable{
	
	int id;
	int m[];
	
	public Hilo3(int id,int m []){
		this.id=id;
		this.m=m;
	}
	
	@Override
	public void run() {
		Random r = new Random(System.currentTimeMillis());
		m[id] = r.nextInt(10);	
	}
	
}
	
	


class Hilo2 implements Runnable{

	private int id;
	
	public Hilo2(int id){
		this.id=id;
	}
	
	
	@Override
	public void run() {
		Random r = new Random(System.currentTimeMillis());
		System.out.println("Soy "+id+" empiezo a calcular...");
		factorial(r.nextInt(100));
		System.out.println("Soy "+id+" Calculado!");
	}
	
	public int factorial (int numero) {
		if (numero==0)
			return 1;
		else
			return (int) (numero * factorial(numero-1));
	}
}

class HiloHijo  extends Hilo{

	public HiloHijo(int nombre, int duracion) {
		super(nombre, duracion);
	}

	@Override
	public void run() {
		System.out.println("Hilo hijo");
		System.out.println("Soy el hilo hijo "+nombre+" y soy "+Thread.currentThread().getName());
		System.out.println("Soy el hilo hijo"+this.nombre+" y he iniciado mi ejecuci�n.");
		System.out.println("Soy el hilo hijo"+this.nombre+" y voy a parar mi ejecuci�n "+this.duracion+" ms.");
		System.out.println("Soy el hilo hijo"+this.nombre+" y contin�o mi ejecuci�n.");
		System.out.println("Soy el hilo hijo"+this.nombre+" y he finalizado mi ejecuci�n.");
		
	}
	
}

class Hilo implements Runnable{
	
	protected int nombre;
	protected int duracion;
	
	public Hilo(int nombre, int duracion){
		this.nombre=nombre;
		this.duracion=duracion;
	}
	
	public void run(){
		System.out.println("Soy el hilo "+this.nombre+" y soy "+Thread.currentThread().getName());
		System.out.println("Soy el hilo "+this.nombre+" y he iniciado mi ejecuci�n.");
		System.out.println("Soy el hilo "+this.nombre+" y voy a parar mi ejecuci�n "+this.duracion+" ms.");
		try{
			HiloHijo h = new HiloHijo(nombre+10,duracion);//clase que se ejecuta en el hilo
			Thread t = new Thread(h);//Hilo(clase a ejecutar en el hilo)
			System.out.println("Hijo arranca");
			t.start();//Inicio la ejecucion
			t.join();//Fuerzo a que se espere a que termine un hilo
			System.out.println("Hijo finalizado, ahora duermo...");
			Thread.sleep(this.duracion);
		}
		catch(Exception e){
			System.out.println(e.toString());
			Logger.getLogger(Hilo.class.getName()).log(Level.SEVERE, null, e);
		}
		System.out.println("Soy el hilo "+this.nombre+" y contin�o mi ejecuci�n.");
		System.out.println("Soy el hilo "+this.nombre+" y he finalizado mi ejecuci�n.");

	}
}


