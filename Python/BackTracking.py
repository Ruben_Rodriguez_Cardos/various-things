import math

def Backtraking(numbers, solutions,partial_solution,stage):
	if stage == len(numbers):
		partial_solution=partial_solution[0:len(numbers)]
		#Con esta solucion parcial se podria calcular cualquier tipo de solucion, permutaciones, beneficios,etc...
		solutions.append(partial_solution)
		partial_solution=[]
	else:
		for i in range(0,2):
			partial_solution[stage]=i
			Backtraking(numbers,solutions,partial_solution,stage+1)
			partial_solution[stage]=0
	return solutions





def main():
	numbers = [1,2]
	aux = [0]*len(numbers)
	solutions=Backtraking(numbers,[],aux,0)
	print("Number of solutions: "+str(len(solutions))+" -> "+str(math.pow(2,len(numbers))))
	for solution in solutions:
		print("---Solution---")
		print(solution)
		print("--------------")
		



if __name__ == "__main__" :
	main()