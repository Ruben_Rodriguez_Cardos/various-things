import pandas as pd
import numpy as np

from sklearn.preprocessing import LabelEncoder

def main():
    
    # Define the headers since the data does not have any
    headers = ["symboling", "normalized_losses", "make", "fuel_type", "aspiration",
            "num_doors", "body_style", "drive_wheels", "engine_location",
            "wheel_base", "length", "width", "height", "curb_weight",
            "engine_type", "num_cylinders", "engine_size", "fuel_system",
            "bore", "stroke", "compression_ratio", "horsepower", "peak_rpm",
            "city_mpg", "highway_mpg", "price"]

    # Read in the CSV file and convert "?" to NaN
    df = pd.read_csv("cars.csv",
                    header=None, names=headers, na_values="?" )
    
    print(df.head())
    #print(df.dtypes)

    #Vamos a categorizar solo las columnas no numericas
    obj_df = df.select_dtypes(include=['object']).copy()
    print(obj_df.head())
    
    #Valores nulos
    obj_df[obj_df.isnull().any(axis=1)]
    print(obj_df.head())

    print(obj_df["num_doors"].value_counts())
    #En la columna num_doors se ponen los valores nan a "four", por ser el mas comun
    obj_df = obj_df.fillna({"num_doors": "four"})

    #Tecnica 1 - Remplazar (Se puede crear un diccionario de reemplazo)
    print(obj_df["num_cylinders"].value_counts())

    cleanup_nums = {"num_doors": {"four": 4, "two": 2},
                "num_cylinders": {"four": 4, "six": 6, "five": 5, "eight": 8, "two": 2, "twelve": 12, "three":3 }}

    obj_df.replace(cleanup_nums, inplace=True)
    print(obj_df.head())
    print(obj_df.dtypes)

    #Tecnica 2 - Convertir en categoria y codificar
    obj_df["body_style"] = obj_df["body_style"].astype('category')
    print(obj_df.dtypes)

    obj_df["body_style_cat"] = obj_df["body_style"].cat.codes
    print(obj_df.head())

    #Tecnica 3 - Scikit-learn Label Encoder
    lb_make = LabelEncoder()
    obj_df["make_code"] = lb_make.fit_transform(obj_df["make"])
    obj_df["fuel_type_code"] = lb_make.fit_transform(obj_df["fuel_type"])
    print(obj_df[["make", "make_code","fuel_type","fuel_type_code"]].head(20))

if __name__ == "__main__":
    main()