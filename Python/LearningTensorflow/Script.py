import pandas as pd


import tensorflow as tf
from tensorflow.keras import layers
from tensorflow import keras
import numpy as np



def createSubmitFile(casos_sj,casos_iq):
    data = pd.read_csv('Data/submission_format.csv')
    
    #Uno los datos, primero san juan y luego iq
    casos=casos_sj+casos_iq
    data['total_cases']=casos
    data.to_csv('Data/submission_format_ready.csv', index=False)
    
def normalize(df):
    result = df.copy()
    for feature_name in df.columns:
        if feature_name != 'year' and feature_name != 'weekofyear' and feature_name != 'total_cases':
            max_value = df[feature_name].max()
            min_value = df[feature_name].min()
            result[feature_name] = (df[feature_name] - min_value) / (max_value - min_value)
    return result
    
def preProcesing():

    #Carga de los datos
    data = pd.read_csv('Data/dengue_features_train.csv')
    #print(data.columns)
    aux = pd.read_csv('Data/dengue_labels_train.csv')
    aux = aux['total_cases'].values.tolist()

    #Union de los datos
    data['total_cases'] = aux

    #Separo los datos por ciudad
    data_sj = data.loc[data['city'] == 'sj']
    data_iq = data.loc[data['city'] == 'iq']

    #Eliminación de columnas innecesarias, redundatos o erroneas
    colsToDrop=['city','week_start_date']
    data_sj=data_sj.drop(columns=colsToDrop)
    data_iq=data_iq.drop(columns=colsToDrop)

    
    #Elimina valores nulos
    data_iq.replace(["NaN", 'NaT'], np.nan, inplace = True)
    data_iq = data_iq.dropna()

    data_sj.replace(["NaN", 'NaT'], np.nan, inplace = True)
    data_sj = data_sj.dropna()

  
    #CARGA DE LOS DATOS DE TEST
    #Carga de los datos
    data = pd.read_csv('Data/dengue_features_test.csv')


    #Separo los datos por ciudad
    data_test_sj = data.loc[data['city'] == 'sj']
    data_test_iq = data.loc[data['city'] == 'iq']

    #Eliminación de columnas innecesarias, redundatos o erroneas
    colsToDrop=['city']
    data_test_sj=data_test_sj.drop(columns=colsToDrop)
    data_test_iq=data_test_iq.drop(columns=colsToDrop)

    #NO SE PUEDEN ELIMINAR LOS REGISTROS ERRONEOS O IMCOMPLETOS DE LOS TEST
    #¿SUSTITUIR/ASUMIR VALORES POR DEFECTO?

    #Elimina valores nulos NO SUSTITUYE?
    data_test_iq.replace(["NaN", 'NaT'], 0, inplace = True)
    data_test_iq.fillna(0, inplace=True)
    #data_test_iq = data_test_iq.dropna()

    data_test_sj.replace(["NaN", 'NaT'], 0, inplace = True)
    data_test_sj.fillna(0, inplace=True)
    #data_test_sj = data_test_sj.dropna()

    #Normalización de los datos
    #NO DEBEN NORMALIZARSE TODAS LAS COLUMNAS
    #AÑO,SEMANA DEL MES,CASOS TOTALES NO DEBEN NORMALIZARSE
    #data_sj = normalize(data_sj)
    #data_iq = normalize(data_iq)

    #data_test_sj = normalize(data_test_sj)
    #data_test_iq = normalize(data_test_iq)

    return data_sj, data_iq,data_test_sj, data_test_iq

def createModel(train_data):
    model = keras.Sequential([
        keras.layers.Dense(64, activation=tf.nn.relu,
                        input_shape=(train_data.shape[1],)),
        keras.layers.Dense(64, activation=tf.nn.relu),
        keras.layers.Dense(1)
    ])

    optimizer = tf.train.RMSPropOptimizer(0.001)

    model.compile(loss='mse',
                    optimizer=optimizer,
                    metrics=['mae'])
    return model

def doRegresion(data,labels,data_test):
    #CREAMOS EL MODELO
    model = createModel(data.values)

    #Entrenamiento del modelo
    model.fit(data.values, labels.values, epochs=10, batch_size=32)

    result = model.predict(data_test, batch_size=32).flatten()

    result = [int(i) for i in result]
    return result

def main():

    data_sj, data_iq, data_test_sj, data_test_iq = preProcesing()
   
    #Por resumir me quedo con las features que ya calcule en su dia, repo dsi-practica2 en bickbucket, las 5 primeras
   
    features = ["weekofyear","station_precip_mm","ndvi_nw","reanalysis_dew_point_temp_k","reanalysis_tdtr_k"]
    
    labels_sj = data_sj["total_cases"]
    data_sj = data_sj[features]
    data_test_sj = data_test_sj[features]

    labels_iq = data_iq["total_cases"]
    data_iq = data_iq[features]
    data_test_iq = data_test_iq[features]
    
    result_sj = doRegresion(data_sj,labels_sj,data_test_sj)
    result_iq = doRegresion(data_iq,labels_iq,data_test_iq)

    createSubmitFile(result_sj,result_iq)

    

if __name__ == "__main__":
    main()