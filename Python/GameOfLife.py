import tkinter as tk
import random as rn

class Application(tk.Frame):
    
    def __init__(self, master=None):
        super().__init__(master)
        self.pack()
        
        self.data = [0]*81
        self.array_btns = [0]*81

        self.create_widgets()

    def create_widgets(self):

    	for i in range(0,len(self.data)):
    		self.data[i]=rn.randrange(0,2) # 0=muertas

    	#3x3 Btn grid

    	for i in range(0,9):
    		for j in range(0,9):
    			self.array_btns[9*i+j]=tk.Button(self)
    			self.array_btns[9*i+j]["text"]="\t\t"
    			if self.data[9*i+j] == 1:
    				self.array_btns[9*i+j]["bg"]="white"
    			else:
    				self.array_btns[9*i+j]["bg"]="black"


    			self.array_btns[9*i+j].grid(column=i,row=j)

    	#Label
    	self.txt = tk.Label(self)
    	self.txt["text"] = "Black = Dead"
    	self.txt.grid(column=2,row=10)

    	self.txt2 = tk.Label(self)
    	self.txt2["text"] = "White = Live"
    	self.txt2.grid(column=2,row=11)

    	#Exit Btn
    	self.exitBtn = tk.Button(self)
    	self.exitBtn["text"] = "Exit"
    	self.exitBtn["fg"] = "red"
    	self.exitBtn["command"] = self.destroy
    	#self.hi_there.pack(side="top")
    	self.exitBtn.grid(column=1,row=11)

    	#Exit Btn
    	self.nextBtn = tk.Button(self)
    	self.nextBtn["text"] = "Next"
    	self.nextBtn["fg"] = "blue"
    	self.nextBtn["command"] = self.next_step
    	#self.hi_there.pack(side="top")
    	self.nextBtn.grid(column=1,row=10)

    
    def say_hi(self):
        print("hi there, everyone!")
        self.hi_there["text"] = "Btn Pressed!"

    def next_step(self):
    	print("Next_Step")
    	
    	#Next step of the simulation
    	for cell in range(0,len(self.data)):
    		live_neighbors=0
    		for i in range(-1,2):
    			for j in range(-1,2):
    				if i == 0 and j == 0:
    					pass
    				else:
    					aux = (9*i)+(j)+cell
    					if aux > 0 and aux < 81:
    						if self.data[aux] == 1: 
    							live_neighbors+=1
    		
    		if live_neighbors == 2  :
    			self.data[cell]=1
    		elif live_neighbors == 3:
    			self.data[cell]=1
    		else:
    			self.data[cell]=0

    	for i in range(0,len(self.array_btns)):
	    	if self.data[i] == 1:
	    		self.array_btns[i]["bg"]="white"
	    	else:
	    		self.array_btns[i]["bg"]="black"  					




def main():
	root = tk.Tk()
	root.title("Game Of Life")
	app = Application(master=root)
	app.mainloop()

if __name__ == "__main__":
	main()