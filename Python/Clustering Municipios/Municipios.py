import pandas as pd
import matplotlib.pyplot as plt
import random as r

from sklearn.cluster import KMeans
from sklearn import metrics

import numpy as np

from sklearn import mixture
import itertools

from sklearn.cluster import KMeans
from scipy.cluster import hierarchy
from sklearn.cluster import AgglomerativeClustering

def loadData(file):
    df = pd.read_csv(file, sep=';')

    y=df["Latitud"].tolist()
    x=df["Longitud"].tolist()
    z=df["Altitud"].tolist()

    i=df["Habitantes"].tolist()
    j=df["Hombres"].tolist()
    k=df["Mujeres"].tolist()
    
    x = [i.replace(",",".") for i in x ]
    y = [i.replace(",",".") for i in y ]
    z = [i.replace(",",".") for i in y ]

    x = [float(i) for i in x ]
    y = [float(i) for i in y ]
    z = [float(i) for i in y ]

    i = [int(i) for i in i ]
    j = [int(i) for i in j ]
    k = [int(i) for i in k ]

    df["Latitud"] = y
    df["Longitud"] = x
    df["Altitud"] = z

    df["Habitantes"] = i
    df["Hombres"] = j
    df["Mujeres"] = k

    return df

def plotMapByRegion(df,region):
    
    aux = df.loc[df['Comunidad'] == region]

    y=aux["Latitud"].tolist()
    x=aux["Longitud"].tolist()

    
    plt.scatter(x,y,alpha=0.5)
    plt.show()

def doCluster(df, region):
    aux = df.loc[df['Comunidad'] == region]


    colsToDrop = ['Comunidad', 'Provincia' ,'Población']
    aux = aux.drop(colsToDrop,1)

    # parameters
    init = 'random' # initialization method 
    iterations = 10 # to run 10 times with different random centroids to choose the final model as the one with the lowest SSE
    max_iter = 300 # maximum number of iterations for each single run
    tol = 1e-04 # controls the tolerance with regard to the changes in the within-cluster sum-squared-error to declare convergence
    random_state = 0 # random

    mat = aux.values

    distortions = []
    silhouettes = []

    for i in range(2, 11):
        km = KMeans(i, init, n_init = iterations ,max_iter= max_iter, tol = tol,random_state = random_state)
        labels = km.fit_predict(mat)
        distortions.append(km.inertia_)
        silhouettes.append(metrics.silhouette_score(mat, labels))
        
        y=aux["Latitud"].tolist()
        x=aux["Longitud"].tolist()

        plt.xlabel('Nº of cluster: '+str(i))
        plt.scatter(x,y,c = labels,alpha=0.5)
        plt.show()

	# Plot Distoritions y Silhouette 
    plt.xlabel('Number of clusters')
    plt.plot( range(2,11), distortions, marker='o', markerfacecolor='blue',label="Distortion")
    plt.plot( range(2,11), silhouettes, marker='o', markerfacecolor='red',label="Silohouette")
    plt.legend()
    plt.show()

def doBIC(df, region, show):
    aux = df.loc[df['Comunidad'] == region]


    colsToDrop = ['Comunidad', 'Provincia' ,'Población',"Altitud","Habitantes"]
    aux = aux.drop(colsToDrop,1)


    #GMM para BIC
    lowest_bic = np.infty
    bic = []
    n_components_range = range(1, 10)
    cv_types = ['spherical', 'tied', 'diag', 'full']
    for cv_type in cv_types:
        for n_components in n_components_range:
            # Fit a Gaussian mixture with EM
            gmm = mixture.GaussianMixture(n_components=n_components,
                                        covariance_type=cv_type)
            gmm.fit(aux)
            bic.append(gmm.bic(aux))
            if bic[-1] < lowest_bic:
                lowest_bic = bic[-1]
                best_gmm = gmm

    bic = np.array(bic)
    color_iter = itertools.cycle(['navy', 'turquoise', 'cornflowerblue',
                                'darkorange'])
    clf = best_gmm
    bars = []

    #aux = clf.get_params()
    #print(aux["n_components"])

    if show == True:

        # Plot the BIC scores
        plt.figure(figsize=(8, 6))
        #spl = plt.subplot(2, 1, 1)
        for i, (cv_type, color) in enumerate(zip(cv_types, color_iter)):
            xpos = np.array(n_components_range) + .2 * (i - 2)
            bars.append(plt.bar(xpos, bic[i * len(n_components_range):
                                        (i + 1) * len(n_components_range)],
                                width=.2, color=color))
        plt.xticks(n_components_range)
        plt.ylim([bic.min() * 1.01 - .01 * bic.max(), bic.max()])
        plt.title('BIC score per model')
        xpos = np.mod(bic.argmin(), len(n_components_range)) + .65 +\
            .2 * np.floor(bic.argmin() / len(n_components_range))
        plt.text(xpos, bic.min() * 0.97 + .03 * bic.max(), '*', fontsize=14)
        plt.xlabel('Number of components')
        #plt.set_xlabel('Number of components')
        plt.legend([b[0] for b in bars], cv_types)
        plt.show()

    return clf.get_params()["n_components"]

def studyBIC(df,region):
    k_s = {}
    for i in range(0,10):
        k = doBIC(df,region,False)
        if k in k_s.keys():
            k_s[k]+=1
        else:
            k_s[k]=1
    plt.bar(range(len(k_s)), list(k_s.values()), align='center')
    plt.xticks(range(len(k_s)), list(k_s.keys()))
    
    plt.xlabel('Frecuencia')
    plt.xlabel('Numero de componentes')
    plt.show()

def doDendogram(data):
    #Dendograma (SCIKIT LEAN NO HACE DENDOGRAMAS)
    Z = hierarchy.linkage(data, 'single')
    plt.figure()
    dn = hierarchy.dendrogram(Z)
    plt.show()

def main():
    df = loadData("Municipios.csv")
    region = "Andalucía"

    #plotMapByRegion(df,region)
    #doCluster(df,region)
    #studyBIC(df,region)

    #Cluster con 9 componentes, es el que el BIC establece como mejor
    aux = df.loc[df['Comunidad'] == region]
    colsToDrop = ['Comunidad', 'Provincia' ,'Población',"Altitud","Habitantes"]
    aux = aux.drop(colsToDrop,1)
    

    #doDendogram(aux)

    #Pruebas con clustering jerarquico (El dendograma se saca de un jerarquico)
    clustering = AgglomerativeClustering(n_clusters = 9).fit(aux)
    aux["Cluster"] = clustering.labels_
    
    #print(aux["Cluster"].min()," - ",aux["Cluster"].max())
    for i in range(aux["Cluster"].min(),aux["Cluster"].max()+1):
        data = aux.loc[aux['Cluster'] == i]
        print("--- Cluster: ",i," ---")
        print(data.describe())
        print("------\n")


    
    




if __name__ == "__main__":
    main()