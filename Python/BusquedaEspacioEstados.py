import queue
import networkx as nx
import matplotlib.pyplot as plt

class Node():

	def __init__(self,id,valor,coste,estado,profundidad,father):
		self.id=id
		self.valor=valor
		self.coste=coste
		self.estado=estado
		self.profundidad=profundidad
		self.father=father

	def __repr__(self):#ToString
		return "Node - Id: "+str(self.id)+" ,Valor: "+str(self.valor)+", Coste: "+str(self.coste)+", Estado: "+self.estado+", Profundidad: "+str(self.profundidad)+", Padre: "+self.father.estado

class NodeGraph():

	def __init__(self,id,name):
		self.id=id
		self.name=name
		

def searchNodeByName(data,name):
	res = data.nodes()[0]
	for node in data.nodes():
		if node.name == name:
			res=node
	return res

def showGraph(data):
	elarge=[(u,v) for (u,v,d) in data.edges(data=True) if d['weight'] >0.5]
	esmall=[(u,v) for (u,v,d) in data.edges(data=True) if d['weight'] <=0.5]

	pos=nx.spring_layout(data) # positions for all nodes

	# nodes
	nx.draw_networkx_nodes(data,pos,node_size=700)

	# edges
	nx.draw_networkx_edges(data,pos,edgelist=elarge,
	                    width=6)
	nx.draw_networkx_edges(data,pos,edgelist=esmall,
	                    width=6,alpha=0.5,edge_color='b',style='dashed')

	# labels
	nx.draw_networkx_labels(data,pos,font_size=20,font_family='sans-serif')

	plt.axis('off')
	plt.show() # display


def sucesores(data,actual_state,n):
	res=[]
	n_id=n
	node = searchNodeByName(data,actual_state.estado)
	
	for (u,v,d) in data.edges(node,data=True):#u=Origen, v=destino, d = peso
		#Valores nuevo nodo
		n_id+=1 #Identificador
		n_coste = actual_state.coste + d["weight"] #Coste
		n_estado = v.name #Estado
		n_profundidad = actual_state.profundidad+1 #Profundidad
		n_father = actual_state
		#Valor se define mediante el algoritmo que vamos a utilizar para la busqueda
		#Usamos Anchura en este caso valor=profundidad+coste 
		n_valor = actual_state.profundidad+1+n_coste #Valor

		#Nuevo Nodo
		aux = Node(n_id,n_valor,n_coste,n_estado,n_profundidad,n_father)#id,valor,coste,estado,profundidad,father
		print(aux.valor,aux)
		res.append(aux)


	return res


def main():
	
	#Cargo nodo que sera el mapa
	data = nx.Graph()
	ng1=NodeGraph(1,"Calle Paloma")
	ng2=NodeGraph(2,"Calle Toledo")
	ng3=NodeGraph(3,"Calle Luz")
	ng4=NodeGraph(4,"Calle Calatrava")
	ng5=NodeGraph(5,"Calle Clavel")

	data.add_edge(ng1,ng2, weight=1)
	data.add_edge(ng2,ng3, weight=1)
	data.add_edge(ng1,ng4, weight=2)
	data.add_edge(ng4,ng5, weight=2)
	data.add_edge(ng5,ng3, weight=2)

	#Busqueda en espacio de Estados
	#Vamos a ir Calle Paloma - Calle Luz
	frontera = queue.PriorityQueue()#Frontera
	initial_node = searchNodeByName(data,"Calle Paloma")
	initial_state = Node(1,0,0,initial_node.name,0,None)#Estado inicial
	final_state = "Calle Luz"#Estado Final
	frontera.put((initial_state.valor,initial_state.id,initial_state))#Añadimos el nodo inicial a la frontera
	final=False #Estado final encontrado
	result=[] #Camino Resultado
	n=1
	while frontera.empty() != True and final==False:
		actual_state=frontera.get()[2]#Selecciono el primer nodo de la frontera
		suc=sucesores(data,actual_state,n)#Genero Sucesores
		for aux in suc:
			#Añadimos los sucesores a la frontera en funcion de: primero su valor y 
			#segundo su id por si aparecen nodos de igual valor, a igual valor el que
			#se genera antes tiene un id menor y lo estudiamos antes
			frontera.put((aux.valor,aux.id,aux))
			n+=1


		if actual_state.estado == final_state:#Si el estado actual es final hemos llegado a la solucion
			final=True#Estado final alcanzado
			#Construyo solucion a traves de actual_state
			while actual_state.father != None:
				result.insert(0,actual_state)
				actual_state=actual_state.father
			result.insert(0,initial_state)
	#Resultado
	print("---Resultado---")
	for aux in result:
		print(aux.estado)
		

if __name__ == "__main__":
	main()