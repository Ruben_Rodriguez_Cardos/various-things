from sklearn.datasets import load_iris
from sklearn import tree
import graphviz 

iris = load_iris()

print(len(iris.data))
print(iris.data)
print(iris.target)
print(iris.target_names)
print(iris.feature_names)



clf = tree.DecisionTreeClassifier()
clf = clf.fit(iris.data, iris.target)
 

dot_data = tree.export_graphviz(clf, out_file=None,feature_names=iris.feature_names,class_names=iris.target_names,filled=True, rounded=True,special_characters=True)  
graph = graphviz.Source(dot_data)
graph.render("iris") 


